package com.developer.InstantAssistanceSeeking.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CompleteWork {
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("complete_by")
    @Expose
    private String completeBy;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCompleteBy() {
        return completeBy;
    }

    public void setCompleteBy(String completeBy) {
        this.completeBy = completeBy;
    }

}
