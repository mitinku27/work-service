package com.developer.InstantAssistanceSeeking.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WorkerDetailsModel {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("experience")
    @Expose
    private String experience;
    @SerializedName("working_hours")
    @Expose
    private String workingHours;
    @SerializedName("expected_salary")
    @Expose
    private String expectedSalary;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public String getWorkingHours() {
        return workingHours;
    }

    public void setWorkingHours(String workingHours) {
        this.workingHours = workingHours;
    }

    public String getExpectedSalary() {
        return expectedSalary;
    }

    public void setExpectedSalary(String expectedSalary) {
        this.expectedSalary = expectedSalary;
    }
}
