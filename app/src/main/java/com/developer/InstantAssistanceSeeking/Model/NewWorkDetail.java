package com.developer.InstantAssistanceSeeking.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NewWorkDetail {
    @SerializedName("user")
    @Expose
    private User user;
    @SerializedName("work")
    @Expose
    private Work work;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Work getWork() {
        return work;
    }

    public void setWork(Work work) {
        this.work = work;
    }

}
