package com.developer.InstantAssistanceSeeking.Model;

public class NewWork {
    Data data;
    workDetailModel model;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public workDetailModel getModel() {
        return model;
    }

    public void setModel(workDetailModel model) {
        this.model = model;
    }
}
