package com.developer.InstantAssistanceSeeking.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Interest {
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("worker_id")
    @Expose
    private String workerId;
    @SerializedName("work_id")
    @Expose
    private String work_id;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getWorkerId() {
        return workerId;
    }

    public void setWorkerId(String workerId) {
        this.workerId = workerId;
    }

    public String getWork_id() {
        return work_id;
    }

    public void setWork_id(String work_id) {
        this.work_id = work_id;
    }
}
