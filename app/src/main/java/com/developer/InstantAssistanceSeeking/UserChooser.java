package com.developer.InstantAssistanceSeeking;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;

import com.developer.InstantAssistanceSeeking.User.LogIn;
import com.developer.InstantAssistanceSeeking.Worker.WorkerHome;
import com.developer.InstantAssistanceSeeking.Worker.WorkerLogin;

public class UserChooser extends AppCompatActivity {
    Button worker,user;
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_chooser);
        textView=findViewById(R.id.text);
        worker=findViewById(R.id.worker);
        user=findViewById(R.id.user);

        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.anticipate_overshoot_interpolator);
        textView.setAnimation(animation);
        Animation animation1 = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.left_enter);
        user.setAnimation(animation1);
        Animation animation2 = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.right_enter);
        worker.setAnimation(animation2);

        user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(UserChooser.this, LogIn.class));
            }
        });
        worker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(UserChooser.this, WorkerLogin.class));

            }
        });

    }
}
