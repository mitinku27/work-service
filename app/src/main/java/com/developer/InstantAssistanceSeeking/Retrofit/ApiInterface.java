package com.developer.InstantAssistanceSeeking.Retrofit;

import com.developer.InstantAssistanceSeeking.Model.AllWorkModel;
import com.developer.InstantAssistanceSeeking.Model.CompleteWork;
import com.developer.InstantAssistanceSeeking.Model.Interest;
import com.developer.InstantAssistanceSeeking.Model.NewWorkDetail;
import com.developer.InstantAssistanceSeeking.Model.Status;
import com.developer.InstantAssistanceSeeking.Model.Type;
import com.developer.InstantAssistanceSeeking.Model.UserLocation;
import com.developer.InstantAssistanceSeeking.Model.UserLogInInfo;
import com.developer.InstantAssistanceSeeking.Model.Worker;
import com.developer.InstantAssistanceSeeking.Model.WorkerDetailsModel;
import com.developer.InstantAssistanceSeeking.Model.workDetailModel;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ApiInterface {
    @POST("usersignup.php")
    @FormUrlEncoded
    Call<Status> userSignUp(
            @Field("email")String email,
            @Field("password")String password,
            @Field("name")String name,
            @Field("phone")String phone,
            @Field("address")String address


    );
    @POST("workersignup.php")
    @FormUrlEncoded
    Call<Status> workerSignUp(
            @Field("email")String email,
            @Field("password")String password,
            @Field("name")String name,
            @Field("phone")String phone,
            @Field("address")String address,
            @Field("type_id")String type_id,
            @Field("availability")String availability


    );


    @POST("userlogin.php")
    @FormUrlEncoded
    Call<UserLogInInfo> userLogin(
            @Field("email")String email,
            @Field("password")String password



    );
    @POST("workerlogin.php")
    @FormUrlEncoded
    Call<UserLogInInfo> workerLogin(
            @Field("email")String email,
            @Field("password")String password



    );
    @POST("setlocation.php")
    @FormUrlEncoded
    Call<Status> setLocation(
            @Field("user_id")String user_id,
            @Field("latitude")String latitude,
            @Field("longitude")String longitude,
            @Field("worker_id")String worker_id,
            @Field("address")String address


    );



    @GET("gettype.php")
    Call<List<Type>> getType();
    @GET("getallwork.php")
    Call<List<AllWorkModel>> getAllWork();


    @POST("gettypename.php")
    @FormUrlEncoded
    Call<List<Type>> getTypeName(
            @Field("id")String worker_id
    );
    @POST("setworkerdetails.php")
    @FormUrlEncoded
    Call<Status> setWorkerDetails(
            @Field("worker_id")String worker_id,
            @Field("experience")String experience,
            @Field("expected_salary")String expected_salary,
            @Field("working_hours")String working_hours

    );

    @POST("getworkerdetails.php")
    @FormUrlEncoded
    Call<WorkerDetailsModel> getWorkerDetails(
            @Field("worker_id")String worker_id

    );


    @POST("setWork.php")
    @FormUrlEncoded
    Call<Status> setWorkRequest(
            @Field("type_id")String type_id,
            @Field("work_description")String work_description,
            @Field("time")String time,
            @Field("location")String location,
            @Field("money")String money,
            @Field("user_id")String user_id


    );

    @POST("setInterest.php")
    @FormUrlEncoded
    Call<Status> setInterest(
            @Field("worker_id")String worker_id,
            @Field("work_id")String work_id,
            @Field("user_id") String user_id,
            @Field("sent_from") String from

    );
    @POST("workerworkrequest.php")
    @FormUrlEncoded
    Call<List<NewWorkDetail>> workerRequst(
            @Field("worker_id")String worker_id

    );
    @POST("userworkrequest.php")
    @FormUrlEncoded
    Call<List<NewWorkDetail>> userRequest(
            @Field("user_id")String user_id

    );
    @POST("checkworkerwork.php")
    @FormUrlEncoded
    Call<List<NewWorkDetail>> checkWorkerRequst(
            @Field("worker_id")String worker_id

    );
    @POST("checkuserworkrequest.php")
    @FormUrlEncoded
    Call<List<NewWorkDetail>> checkUserRequst(
            @Field("user_id")String worker_id

    );

    @POST("getuserworkid.php")
    @FormUrlEncoded
    Call<Status> getWorkId(
            @Field("user_id")String user_id

    );
    @POST("workercompletework.php")
    @FormUrlEncoded
    Call<Status> completeWorkWorker(
            @Field("work_request_id")String work_id,
            @Field("rating")String rating

    );
    @POST("usercompletework.php")
    @FormUrlEncoded
    Call<Status> usercompleteWork(
            @Field("work_request_id")String work_id,
            @Field("rating")String rating

    );
    @POST("checkcomplete.php")
    @FormUrlEncoded
    Call<CompleteWork> CompleteWork(
            @Field("work_id")String work_id


    );

    @POST("getusserdetails.php")
    @FormUrlEncoded
    Call<UserLogInInfo> userInfo(
            @Field("user_id")String user_id



    );
    @POST("getwork.php")
    @FormUrlEncoded
    Call<workDetailModel> geWork(
            @Field("id")String id

    );
    @POST("getallselectedworker.php")
    @FormUrlEncoded
    Call<List<Worker>> getAllWorker(
      @Field("type_id") String type_id
    );

    @POST("acceptwork.php")
    @FormUrlEncoded
    Call<Status> acceptWork(
            @Field("work_id")String work_id,
            @Field("worker_id")String worker_id,
            @Field("user_id")String user_id

    );
    @POST("workerreport.php")
    @FormUrlEncoded
    Call<List<NewWorkDetail>> workerReport(
            @Field("worker_id")String worker_id

    );
    @POST("userReport.php")
    @FormUrlEncoded
    Call<List<NewWorkDetail>> userReport(
            @Field("user_id")String worker_id

    );
    @POST("getlocationworker.php")
    @FormUrlEncoded
    Call<UserLocation> getLocation(
            @Field("worker_id") String id);


}
