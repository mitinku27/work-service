package com.developer.InstantAssistanceSeeking.User;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.developer.InstantAssistanceSeeking.Model.Type;
import com.developer.InstantAssistanceSeeking.R;
import com.haozhang.lib.SlantedTextView;

import java.util.List;

public class AdapterforCategory extends RecyclerView.Adapter<AdapterforCategory.ViewHolder> {
    Context context;
    List<Type> categoryModelList;

     AdapterforCategory(Context context, List<Type> categoryModelList) {
        this.context=context;
        this.categoryModelList=categoryModelList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_category_row,viewGroup,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        final Type t= categoryModelList.get(i);
        viewHolder.stv2.setText(t.getName());
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {

        Intent intent= new Intent(context,NearestWorkerInmap.class);
        intent.putExtra("id",t.getId());
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }
});
    }

    @Override
    public int getItemCount() {
        return categoryModelList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView stv2;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    context.startActivity(new Intent(context,CateGoryDetails.class));
                }
            });
             stv2 =  itemView.findViewById(R.id.name);


        }
    }
}
