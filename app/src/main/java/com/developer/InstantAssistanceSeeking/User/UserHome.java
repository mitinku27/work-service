package com.developer.InstantAssistanceSeeking.User;

import android.Manifest;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.util.Linkify;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.developer.InstantAssistanceSeeking.Model.CompleteWork;
import com.developer.InstantAssistanceSeeking.Model.Data;
import com.developer.InstantAssistanceSeeking.Model.NewWorkDetail;
import com.developer.InstantAssistanceSeeking.Model.Status;
import com.developer.InstantAssistanceSeeking.Model.User;
import com.developer.InstantAssistanceSeeking.Model.Work;
import com.developer.InstantAssistanceSeeking.R;
import com.developer.InstantAssistanceSeeking.Retrofit.ApiClient;
import com.developer.InstantAssistanceSeeking.Retrofit.ApiInterface;
import com.developer.InstantAssistanceSeeking.SharedPref;
import com.developer.InstantAssistanceSeeking.UserChooser;
import com.developer.InstantAssistanceSeeking.Worker.WorkerHome;
import com.haozhang.lib.SlantedTextView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserHome extends AppCompatActivity {
    ImageView card1,card2;
    Toolbar userHomeToolbar;
    Button currentwork;
    CardView search,report,work;
    Handler handler;
    Runnable run;
    static int counter=1;
    static int cc=1;
    static int dd=1;
    List<NewWorkDetail> newWorkDetailList=new ArrayList<>();
    NewWorkDetail newWorkDetail;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_home);

        currentwork=findViewById(R.id.currentwork);
        userHomeToolbar=findViewById(R.id.userHomeToolbar);
       // currentwork.setVisibility(View.GONE);
        card1=findViewById(R.id.card1);
        card2=findViewById(R.id.card2);
        search=findViewById(R.id.searchWork);
        report=findViewById(R.id.reportCard);
        work=findViewById(R.id.workCard);
        userHomeToolbar.setTitle("Home");
        userHomeToolbar.setTitleTextColor(getResources().getColor(R.color.cardview_light_background));
        userHomeToolbar.setNavigationIcon(R.drawable.ic_home_black_24dp);
        setSupportActionBar(userHomeToolbar);

        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(UserHome.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);

        }

        currentwork.setVisibility(View.GONE);
        final SharedPref sharedPref=new SharedPref(this);
        final Data data=sharedPref.getMember();



        final ApiInterface apiInterface= ApiClient.getApiClient().create(ApiInterface.class);




        handler=new Handler();
        run=new Runnable() {

            @Override
            public void run() {

                if (sharedPref.getWork()==null)
                {
                    Call<Status> call=apiInterface.getWorkId(data.getId());
                    call.enqueue(new Callback<Status>() {
                        @Override
                        public void onResponse(Call<Status> call, Response<Status> response) {
                            Status status=response.body();
                            if (!status.getStatus().equals("0"))
                                sharedPref.setWork(status.getStatus());

                        }

                        @Override
                        public void onFailure(Call<Status> call, Throwable t) {
                            currentwork.setVisibility(View.GONE);
                        }
                    });
                }

                Call<List<NewWorkDetail>> call2=apiInterface.checkUserRequst(data.getId());
                NewWorkDetail check=sharedPref.getWorkDetail();

                if (check==null)
                {
                    call2.enqueue(new Callback<List<NewWorkDetail>>() {
                        @Override
                        public void onResponse(Call<List<NewWorkDetail>> call, Response<List<NewWorkDetail>> response) {
                            newWorkDetailList=response.body();
                            for (int i=0;i<newWorkDetailList.size();i++)
                            {
                                newWorkDetail=newWorkDetailList.get(i);
                                sharedPref.setWorkDetail(newWorkDetail);
                                if (counter==1)
                                {
                                    showNotification("your request has been accepted");
                                    counter++;
                                }

                            }

                            if (newWorkDetail.getWork()!=null)
                            {
                                currentwork.setVisibility(View.VISIBLE);
                                recreate();
                            }


                        }

                        @Override
                        public void onFailure(Call<List<NewWorkDetail>> call, Throwable t) {
                            // currentwork.setVisibility(View.GONE);
                        }
                    });
                }else {
                    currentwork.setVisibility(View.VISIBLE);
                    Work work=check.getWork();
                    Call<CompleteWork> completeWorkCall=apiInterface.CompleteWork(work.getId());
                    completeWorkCall.enqueue(new Callback<CompleteWork>() {
                        @Override
                        public void onResponse(Call<CompleteWork> call, Response<CompleteWork> response) {
                            CompleteWork completeWork=response.body();
                            if (completeWork.getStatus().equals("2"))
                            {
                                if (dd==1)
                                {
                                    showNotification("User Completed the work");
                                    dd++;
                                }

                            }
                        }

                        @Override
                        public void onFailure(Call<CompleteWork> call, Throwable t) {

                        }
                    });
                }
                Call<List<NewWorkDetail>> call3=apiInterface.userRequest(data.getId());

                call3.enqueue(new Callback<List<NewWorkDetail>>() {
                    @Override
                    public void onResponse(Call<List<NewWorkDetail>> call, Response<List<NewWorkDetail>> response) {
                        newWorkDetailList=response.body();
                        if (cc==1)
                        {
                            showNotification("Someone sent a request");
                            cc++;
                        }

                    }

                    @Override
                    public void onFailure(Call<List<NewWorkDetail>> call, Throwable t) {

                    }
                });

                handler.postDelayed(this,10000);

            }
        };
        handler.postDelayed(run, 12000);




        SlantedTextView stv1 = (SlantedTextView) findViewById(R.id.test1);
        SlantedTextView stv2 = (SlantedTextView) findViewById(R.id.test2);
        SlantedTextView stv3 = (SlantedTextView) findViewById(R.id.test3);

        stv2.setText("Work Report")
                .setTextColor(Color.WHITE)
                .setSlantedBackgroundColor(getResources().getColor(R.color.rippelColor))

                .setTextSize(18)
                .setSlantedLength(100)
                .setMode(SlantedTextView.MODE_LEFT);

        stv1.setText("Worker Category")
                .setTextColor(Color.WHITE)
                .setSlantedBackgroundColor(getResources().getColor(R.color.rippelColor))

                .setTextSize(18)
                .setSlantedLength(100)
                .setMode(SlantedTextView.MODE_LEFT);
        stv3.setText("Add Work")
                .setTextColor(Color.WHITE)
                .setSlantedBackgroundColor(getResources().getColor(R.color.rippelColor))

                .setTextSize(18)
                .setSlantedLength(100)
                .setMode(SlantedTextView.MODE_LEFT);


        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(UserHome.this,WorkerCategory.class));
            }
        });
        report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(UserHome.this,UserReport.class));
            }
        });
        work.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(UserHome.this,AddWork.class));

            }
        });


        currentwork.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newWorkDetail = sharedPref.getWorkDetail();
                User user = newWorkDetail.getUser();
                final Work work = newWorkDetail.getWork();
                final Dialog dialog = new Dialog(UserHome.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setCancelable(true);
                dialog.setContentView(R.layout.cell_content_layout);
                TextView dialogButton = dialog.findViewById(R.id.accept);
                TextView name = dialog.findViewById(R.id.name);
                TextView email = dialog.findViewById(R.id.email);

                TextView phone = dialog.findViewById(R.id.phone);
                TextView address = dialog.findViewById(R.id.address);
                TextView money = dialog.findViewById(R.id.money);
                TextView duration = dialog.findViewById(R.id.duration);
                name.setText(user.getName());
                email.setText(user.getEmail());
                if (user.getPhone().charAt(0)!='+')
                {
                    phone.setText("+88"+user.getPhone());
                }else {
                    phone.setText(user.getPhone());
                }
                Linkify.addLinks(phone,Linkify.PHONE_NUMBERS);
                address.setText(user.getAddress());
                money.setText(work.getMoney()+" taka");
                duration.setText(work.getTime()+" days");
                dialogButton.setVisibility(View.VISIBLE);
                dialogButton.setText("Complete Work");
                String work_id=work.getId();
                dialogButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Call<Status> statusCall = apiInterface.usercompleteWork(work.getId(), "4");
                        statusCall.enqueue(new Callback<Status>() {
                            @Override
                            public void onResponse(Call<Status> call, Response<Status> response) {
                                Status status = response.body();
                                if (status.getStatus().equals("1")) {
                                    sharedPref.setWorkDetail(null);
                                    sharedPref.setWork("0");
                                    startActivity(new Intent(UserHome.this, UserHome.class));

                                }
                            }

                            @Override
                            public void onFailure(Call<Status> call, Throwable t) {

                            }
                        });
                    }
                });

                dialog.show();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.user_menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.logout:
                SharedPref sharedPref = new SharedPref(UserHome.this);
                sharedPref.setProfile("0");
                sharedPref.setMember(null);
                sharedPref.setWorkDetail(null);
                sharedPref.setWork("0");
                sharedPref.setPro("0");
                startActivity(new Intent(UserHome.this, UserChooser.class));
                return true;
            case R.id.profile:
                startActivity(new Intent(UserHome.this, UserProfile.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    void showNotification(String msg)
    {

        Intent intent = new Intent("com.example.notitfications.MYNOTIFICATION");

        PendingIntent pendingIntent = PendingIntent.getActivity(UserHome.this, 1, intent, 0);

        Notification.Builder builder = new Notification.Builder(UserHome.this);

        builder.setAutoCancel(false);

        builder.setContentTitle(msg);
        builder.setContentText("Go to App to see details");
        builder.setSmallIcon(R.drawable.blue_button_background);
        builder.setContentIntent(pendingIntent);
        builder.setAutoCancel(true);
        builder.setOngoing(true);

        builder.setVibrate(new long[] { 1000, 1000, 1000, 1000, 1000 });


        builder.setLights(Color.RED, 3000, 3000);


        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
        if(alarmSound == null){
            alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
            if(alarmSound == null){
                alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            }
        }
        builder.setSound(alarmSound);


        builder.setNumber(100);
        builder.build();

        Notification myNotication = builder.getNotification();
        NotificationManager manager= (NotificationManager) UserHome.this.getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(11, myNotication);

    }
}
