package com.developer.InstantAssistanceSeeking.User;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.developer.InstantAssistanceSeeking.Model.Data;
import com.developer.InstantAssistanceSeeking.Model.NewWorkDetail;
import com.developer.InstantAssistanceSeeking.Model.Status;
import com.developer.InstantAssistanceSeeking.Model.User;
import com.developer.InstantAssistanceSeeking.Model.Work;
import com.developer.InstantAssistanceSeeking.R;
import com.developer.InstantAssistanceSeeking.Retrofit.ApiClient;
import com.developer.InstantAssistanceSeeking.Retrofit.ApiInterface;
import com.developer.InstantAssistanceSeeking.SharedPref;
import com.developer.InstantAssistanceSeeking.Worker.NewRequestRecycler;
import com.developer.InstantAssistanceSeeking.Worker.WorkerHome;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserNewRequestRecycler  extends RecyclerView.Adapter<UserNewRequestRecycler.ViewHolder> {
    Context context;
    List<NewWorkDetail> newWorkList;
    ApiInterface apiInterface;
    public UserNewRequestRecycler(Context context, List<NewWorkDetail> newWorkList) {
        this.context=context;
        this.newWorkList=newWorkList;
        apiInterface= ApiClient.getApiClient().create(ApiInterface.class);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cell_content_layout,viewGroup,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        final NewWorkDetail work=newWorkList.get(i);
        final User userData=work.getUser();
        final Work model=work.getWork();
        viewHolder.name.setText(userData.getName());
        viewHolder.email.setText(userData.getEmail());
        viewHolder.phone.setText(userData.getPhone());
        viewHolder.duration.setText(model.getTime());
        viewHolder.money.setText(model.getMoney());
        viewHolder.address.setText(userData.getAddress());
        final SharedPref sharedPref=new SharedPref(context);
        final Data data=sharedPref.getMember();

        if (sharedPref.getWorkDetail()!=null)
        {
            viewHolder.accept.setVisibility(View.GONE);
            viewHolder.accept.setClickable(false);
        }else {
            viewHolder.accept.setVisibility(View.VISIBLE);
            viewHolder.accept.setClickable(true);
        }
        String work_id= model.getId();
        String worker_id=data.getId();
        String user_id=userData.getId();
        viewHolder.accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Call<Status> call=apiInterface.acceptWork(model.getId(),userData.getId(),data.getId());
                call.enqueue(new Callback<Status>() {
                    @Override
                    public void onResponse(Call<Status> call, Response<Status> response) {
                        Status status=response.body();
                        if (status.getStatus().equals("1"))
                        {
                            sharedPref.setWorkDetail(work);
                            sharedPref.setInterest("0");
                            Intent intent=new Intent(context, UserHome.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            context.startActivity(intent);
                        }
                    }

                    @Override
                    public void onFailure(Call<Status> call, Throwable t) {

                    }
                });
            }
        });
    }

    @Override
    public int getItemCount() {
        return newWorkList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView name,email,phone,address,accept,money,duration;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            name=itemView.findViewById(R.id.name);
            email=itemView.findViewById(R.id.email);
            phone=itemView.findViewById(R.id.phone);
            address=itemView.findViewById(R.id.address);
            accept=itemView.findViewById(R.id.accept);
            money=itemView.findViewById(R.id.money);
            duration=itemView.findViewById(R.id.duration);
        }
    }
}
