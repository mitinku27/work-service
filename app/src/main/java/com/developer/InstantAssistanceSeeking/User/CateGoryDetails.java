package com.developer.InstantAssistanceSeeking.User;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.developer.InstantAssistanceSeeking.R;
import com.ramotion.foldingcell.FoldingCell;

import java.util.ArrayList;
import java.util.List;

public class CateGoryDetails extends AppCompatActivity {
    Toolbar toolbar;
    String id;
    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cate_gory_details);
        Intent intent=getIntent();
        id=intent.getStringExtra("id");


        toolbar=findViewById(R.id.categorydeailsToolbar);
        toolbar.setTitle("Worker Details");
        toolbar.setTitleTextColor(getResources().getColor(R.color.cardview_light_background));
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_keyboard_backspace_black_24dp));


        setSupportActionBar(toolbar);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);



        final ListView theListView = findViewById(R.id.listRecycler);
        List<WorkerDetailsModel> modelList=new ArrayList<>();
        for (int i=0;i<10;i++)
        {
            WorkerDetailsModel model=new WorkerDetailsModel();
            modelList.add(model);
        }

        final FoldingCellListAdapter adapter = new FoldingCellListAdapter(CateGoryDetails.this, modelList);
        theListView.setAdapter(adapter);

        // set on click event listener to list view
        theListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                // toggle clicked cell state
                ((FoldingCell) view).toggle(false);
                // register in adapter that state for selected cell is toggled
                adapter.registerToggle(pos);
            }
        });


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
