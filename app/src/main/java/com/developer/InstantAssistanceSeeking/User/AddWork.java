package com.developer.InstantAssistanceSeeking.User;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.developer.InstantAssistanceSeeking.JSONParser;
import com.developer.InstantAssistanceSeeking.Model.Data;
import com.developer.InstantAssistanceSeeking.Model.Status;
import com.developer.InstantAssistanceSeeking.Model.Type;
import com.developer.InstantAssistanceSeeking.R;
import com.developer.InstantAssistanceSeeking.Retrofit.ApiClient;
import com.developer.InstantAssistanceSeeking.Retrofit.ApiInterface;
import com.developer.InstantAssistanceSeeking.SharedPref;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class AddWork extends AppCompatActivity {
    ApiInterface apiInterface;
    RequestQueue queue;
    TextInputEditText amount,duration,description;
    AutoCompleteTextView address;
    Button submit;
    TextView alterlog;
    JSONArray placeids ;
    JSONArray predsJsonArray;
    LatLng myPosition;
    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    private static final String OUT_JSON = "/json";
    private static final String API_KEY = "AIzaSyD7LPuMRoLTZwNZtlUB2K9KLDTZ-kH5Bh0";
    Double latitude,longitude;
    int ids[]=new int[100];
    int selectedId;
    JSONParser jparser=new JSONParser();
    List<Type> typeList= new ArrayList<>();
    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Toolbar userHomeToolbar;
        setContentView(R.layout.activity_add_work);


        amount=findViewById(R.id.amount);
        duration=findViewById(R.id.duration);
        address= findViewById(R.id.address);
        description=findViewById(R.id.description);
        submit=findViewById(R.id.submit);
        userHomeToolbar=findViewById(R.id.toolbar);
        userHomeToolbar.setTitle("Add Work");
        userHomeToolbar.setTitleTextColor(getResources().getColor(R.color.cardview_light_background));
        userHomeToolbar.setNavigationIcon(R.drawable.ic_home_black_24dp);
        setSupportActionBar(userHomeToolbar);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final Spinner dropdown = findViewById(R.id.spinner1);

        apiInterface= ApiClient.getApiClient().create(ApiInterface.class);
        final ArrayList<String> items = new ArrayList<>();


        address.setAdapter(new PostedVoteAdapter(getApplicationContext(), R.layout.tag_places_list));

        address.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String locationtitle = (String) parent.getItemAtPosition(position);

                try{
                    getAddLatLon ss = new getAddLatLon();
                    myPosition= ss.execute(predsJsonArray.getJSONObject(position).getString("place_id")).get();
                    latitude=myPosition.latitude;
                    longitude=myPosition.longitude;

                }catch (Exception e){
                    e.printStackTrace();
                }
                address.setText(locationtitle);
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }

        });

        queue= Volley.newRequestQueue(this);

        JsonArrayRequest request= new JsonArrayRequest(Request.Method.GET, "https://emergencycalling.000webhostapp.com/gettype.php", new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                    try {
                        for (int i=0;i<response.length();i++)
                        {
                            JSONObject object= (JSONObject) response.get(i);
                            items.add(object.getString("name"));
                            ids[i]= Integer.parseInt(object.getString("id"));
                        }
                        dropdown.setAdapter(new ArrayAdapter<String>(getApplicationContext(),android.R.layout.simple_spinner_dropdown_item, items));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        queue.add(request);

        dropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedId=ids[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        final SharedPref sharedPref=new SharedPref(this);
        final Data data=sharedPref.getMember();
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String location=latitude+"@"+longitude;
                Call<Status> call=apiInterface.setWorkRequest(String.valueOf(selectedId),description.getText().toString(),
                        duration.getText().toString(),location,amount.getText().toString(),data.getId());
                call.enqueue(new Callback<Status>() {
                    @Override
                    public void onResponse(Call<Status> call, retrofit2.Response<Status> response) {
                        Status status=response.body();
                        Toast.makeText(getApplicationContext(),"Success",Toast.LENGTH_LONG).show();
                        sharedPref.setWork(status.getStatus());
                        startActivity(new Intent(AddWork.this,UserHome.class));
                    }

                    @Override
                    public void onFailure(Call<Status> call, Throwable t) {

                    }
                });
            }
        });


    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }


    class PostedVoteAdapter extends ArrayAdapter<String> implements Filterable {
        private ArrayList<String> resultList;

        public PostedVoteAdapter(Context context, int textViewResourceId) {
            super(context, textViewResourceId);
        }

        @Override
        public int getCount() {
            return resultList.size();
        }

        @Override
        public String getItem(int index) {
            return resultList.get(index);
        }

        @Override
        public Filter getFilter() {
            Filter filter = new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults filterResults = new FilterResults();
                    if (constraint != null) {
                        // Retrieve the autocomplete results.
                        resultList = autocomplete(constraint.toString());

                        // Assign the data to the FilterResults
                        filterResults.values = resultList;
                        filterResults.count = resultList.size();
                    }
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    if (results != null && results.count > 0) {
                        Log.e("results", String.valueOf(results));
                        notifyDataSetChanged();
                    } else {
                        notifyDataSetInvalidated();
                    }
                }
            };
            return filter;
        }

        public ArrayList<String> autocomplete(String input) {

            ArrayList<String> resultList = null;

            HttpURLConnection conn = null;
            StringBuilder jsonResults = new StringBuilder();
            try {
                StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
                sb.append("?key=" + API_KEY);
                //sb.append("&components=country:QA");
                sb.append("&input=" + URLEncoder.encode(input, "utf8"));

                URL url = new URL(sb.toString());

                System.out.println("URL: "+url);
                conn = (HttpURLConnection) url.openConnection();
                InputStreamReader in = new InputStreamReader(conn.getInputStream());

                // Load the results into a StringBuilder
                int read;
                char[] buff = new char[1024];
                while ((read = in.read(buff)) != -1) {
                    jsonResults.append(buff, 0, read);
                }
            } catch (MalformedURLException e) {
                return resultList;
            } catch (IOException e) {
                return resultList;
            } finally {
                if (conn != null) {
                    conn.disconnect();
                }
            }

            try {

                // Create a JSON object hierarchy from the results
                JSONObject jsonObj = new JSONObject(jsonResults.toString());
                predsJsonArray = jsonObj.getJSONArray("predictions");
                Log.e("jsonObj", String.valueOf(jsonObj));

                // Extract the Place descriptions from the results
                resultList = new ArrayList<String>(predsJsonArray.length());
                placeids = new JSONArray();
                for (int i = 0; i < predsJsonArray.length(); i++) {

                    System.out.println(predsJsonArray.getJSONObject(i).getString("place_id"));
                    System.out.println(predsJsonArray.getJSONObject(i).getString("description"));
                    System.out.println(predsJsonArray.getJSONObject(i).getString("terms"));
                    System.out.println("============================================================");
                    //resultList.add(predsJsonArray.getJSONObject(i).getString("description")+ "              #" +predsJsonArray.getJSONObject(i).getString("place_id"));
                    resultList.add(predsJsonArray.getJSONObject(i).getString("description"));

                }
            } catch (JSONException e) {
            }

            return resultList;
        }


    }





    class getAddLatLon extends AsyncTask<String, String, LatLng> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected LatLng doInBackground(String... para) {

            JSONObject json=jparser.makeHttpRequest("https://maps.googleapis.com/maps/api/place/details/json?placeid="+para[0]+"&key="+API_KEY, "GET",null);
            LatLng latlng = null;
            try {
                JSONObject jso = new JSONObject(json.toString());
                latlng= new LatLng(jso.getJSONObject("result").getJSONObject("geometry").getJSONObject("location").getDouble("lat"),
                        jso.getJSONObject("result").getJSONObject("geometry").getJSONObject("location").getDouble("lng"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return latlng;
        }

        @Override
        protected void onPostExecute(LatLng result) {
            super.onPostExecute(result);

        }
    }



}
