package com.developer.InstantAssistanceSeeking.User;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.ArrayAdapter;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.developer.InstantAssistanceSeeking.Model.Type;
import com.developer.InstantAssistanceSeeking.R;
import com.developer.InstantAssistanceSeeking.Retrofit.ApiClient;
import com.developer.InstantAssistanceSeeking.Retrofit.ApiInterface;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class WorkerCategory extends AppCompatActivity {
    Toolbar toolbar;
    RecyclerView recyclerView;
    ApiInterface apiInterface;
    List<Type> categoryModelList;
    RequestQueue queue;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_worker_category);

        toolbar=findViewById(R.id.workerCatagoryToolbar);
        recyclerView=findViewById(R.id.catagoryRecycler);
        categoryModelList= new ArrayList<>();
        toolbar.setTitle("Category");
        toolbar.setTitleTextColor(getResources().getColor(R.color.cardview_light_background));
        toolbar.setNavigationIcon(R.drawable.ic_backend);
        apiInterface= ApiClient.getApiClient().create(ApiInterface.class);


        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(WorkerCategory.this,2));
        queue= Volley.newRequestQueue(this);

        JsonArrayRequest request= new JsonArrayRequest(Request.Method.GET, "https://emergencycalling.000webhostapp.com/gettype.php", new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                try {
                    for (int i=0;i<response.length();i++)
                    {
                        JSONObject object= (JSONObject) response.get(i);
                        Type t= new Type();
                        t.setId(object.getString("id"));
                        t.setName(object.getString("name"));
                        t.setDescription(object.getString("description"));
                        categoryModelList.add(t);
                    }
                    AdapterforCategory afc=new AdapterforCategory(WorkerCategory.this,categoryModelList);
                    recyclerView.setAdapter(afc);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        queue.add(request);





    }

}

