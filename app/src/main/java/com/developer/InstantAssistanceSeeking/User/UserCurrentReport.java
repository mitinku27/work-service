package com.developer.InstantAssistanceSeeking.User;

import android.annotation.SuppressLint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.developer.InstantAssistanceSeeking.Model.Data;
import com.developer.InstantAssistanceSeeking.Model.NewWorkDetail;
import com.developer.InstantAssistanceSeeking.R;
import com.developer.InstantAssistanceSeeking.Retrofit.ApiClient;
import com.developer.InstantAssistanceSeeking.Retrofit.ApiInterface;
import com.developer.InstantAssistanceSeeking.SharedPref;
import com.developer.InstantAssistanceSeeking.Worker.NewRequestRecycler;
import com.developer.InstantAssistanceSeeking.Worker.WorkerNewRequest;
import com.ramotion.foldingcell.FoldingCell;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserCurrentReport extends AppCompatActivity {
Toolbar userHomeToolbar;
ApiInterface apiInterface;
RecyclerView recyclerView;
    List<NewWorkDetail> newWorkDetailList=new ArrayList<>();
    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_current_report);

        userHomeToolbar=findViewById(R.id.toolbar);
        userHomeToolbar.setTitle("User New Request");
        userHomeToolbar.setTitleTextColor(getResources().getColor(R.color.cardview_light_background));
        userHomeToolbar.setNavigationIcon(R.drawable.ic_keyboard_backspace_black_24dp);
        setSupportActionBar(userHomeToolbar);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        recyclerView=findViewById(R.id.listRecycler);

        apiInterface= ApiClient.getApiClient().create(ApiInterface.class);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        SharedPref sharedPref=new SharedPref(this);
        final Data data=sharedPref.getMember();

        Call<List<NewWorkDetail>> call=apiInterface.userRequest(data.getId());

        call.enqueue(new Callback<List<NewWorkDetail>>() {
            @Override
            public void onResponse(Call<List<NewWorkDetail>> call, Response<List<NewWorkDetail>> response) {
                newWorkDetailList=response.body();
                UserNewRequestRecycler requestRecycler=new UserNewRequestRecycler(UserCurrentReport.this,newWorkDetailList);
                recyclerView.setAdapter(requestRecycler);
            }

            @Override
            public void onFailure(Call<List<NewWorkDetail>> call, Throwable t) {

            }
        });

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

}
