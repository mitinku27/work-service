package com.developer.InstantAssistanceSeeking.User;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;

import com.developer.InstantAssistanceSeeking.R;
import com.haozhang.lib.SlantedTextView;

public class UserReport extends AppCompatActivity {
    ImageView card1,card2;
    Toolbar userHomeToolbar;
    CardView search,report;

    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_report);

        userHomeToolbar=findViewById(R.id.userHomeToolbar);
        userHomeToolbar.setTitle("Report");
        userHomeToolbar.setTitleTextColor(getResources().getColor(R.color.cardview_light_background));
        userHomeToolbar.setNavigationIcon(R.drawable.ic_home_black_24dp);
        setSupportActionBar(userHomeToolbar);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        card1=findViewById(R.id.card1);
        card2=findViewById(R.id.card2);
        search=findViewById(R.id.searchWork);
        report=findViewById(R.id.reportCard);



        SlantedTextView stv1 = (SlantedTextView) findViewById(R.id.test1);
        SlantedTextView stv2 = (SlantedTextView) findViewById(R.id.test2);

        stv2.setText("History")
                .setTextColor(Color.WHITE)
                .setSlantedBackgroundColor(getResources().getColor(R.color.rippelColor))

                .setTextSize(18)
                .setSlantedLength(100)
                .setMode(SlantedTextView.MODE_LEFT);

        stv1.setText("New Request")
                .setTextColor(Color.WHITE)
                .setSlantedBackgroundColor(getResources().getColor(R.color.rippelColor))

                .setTextSize(18)
                .setSlantedLength(100)
                .setMode(SlantedTextView.MODE_LEFT);

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(UserReport.this,UserCurrentReport.class));
            }
        });
        report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(UserReport.this,UserHistory.class));

            }
        });

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }
}
