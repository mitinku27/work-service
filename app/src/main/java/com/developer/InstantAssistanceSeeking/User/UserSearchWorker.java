package com.developer.InstantAssistanceSeeking.User;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;

import com.developer.InstantAssistanceSeeking.R;
import com.haozhang.lib.SlantedTextView;

public class UserSearchWorker extends AppCompatActivity {
    ImageView card1,card2;
    Toolbar userHomeToolbar;
    CardView map,list;
    String id;
    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_search_worker);

        Intent intent=getIntent();
        id=intent.getStringExtra("id");

        userHomeToolbar=findViewById(R.id.userHomeToolbar);

        card1=findViewById(R.id.card1);
        card2=findViewById(R.id.card2);
        map=findViewById(R.id.map);
        list=findViewById(R.id.list);
        userHomeToolbar.setTitle(" Home");
        userHomeToolbar.setTitleTextColor(getResources().getColor(R.color.cardview_light_background));
        userHomeToolbar.setNavigationIcon(R.drawable.ic_home_black_24dp);
        setSupportActionBar(userHomeToolbar);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        SlantedTextView stv1 = (SlantedTextView) findViewById(R.id.test1);
        SlantedTextView stv2 = (SlantedTextView) findViewById(R.id.test2);

        stv2.setText("Show In List")
                .setTextColor(Color.WHITE)
                .setSlantedBackgroundColor(getResources().getColor(R.color.rippelColor))

                .setTextSize(18)
                .setSlantedLength(100)
                .setMode(SlantedTextView.MODE_LEFT);

        stv1.setText("Show in Map")
                .setTextColor(Color.WHITE)
                .setSlantedBackgroundColor(getResources().getColor(R.color.rippelColor))

                .setTextSize(18)
                .setSlantedLength(100)
                .setMode(SlantedTextView.MODE_LEFT);
        list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1=new Intent(UserSearchWorker.this,CateGoryDetails.class);
                intent1.putExtra("id",id);
                startActivity(intent1);
            }
        });
        map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1=new Intent(UserSearchWorker.this,NearestWorkerInmap.class);
                intent1.putExtra("id",id);
                startActivity(intent1);
            }
        });
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

}
