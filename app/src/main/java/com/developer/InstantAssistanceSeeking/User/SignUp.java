package com.developer.InstantAssistanceSeeking.User;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import android.widget.Toast;

import com.developer.InstantAssistanceSeeking.JSONParser;
import com.developer.InstantAssistanceSeeking.Model.Status;
import com.developer.InstantAssistanceSeeking.R;
import com.developer.InstantAssistanceSeeking.Retrofit.ApiClient;
import com.developer.InstantAssistanceSeeking.Retrofit.ApiInterface;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static java.security.AccessController.getContext;

public class SignUp extends AppCompatActivity {
    EditText name,email,phone,password;
    AutoCompleteTextView address;
    Button signup;
    TextView alterlog;
    JSONArray placeids ;
    JSONArray predsJsonArray;
    LatLng myPosition;
    JSONParser jparser=new JSONParser();
    private ApiInterface apiInterface;

    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    private static final String OUT_JSON = "/json";
    private static final String API_KEY = "AIzaSyD7LPuMRoLTZwNZtlUB2K9KLDTZ-kH5Bh0";
    Double latitude,longitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        name= findViewById(R.id.name);
        email= findViewById(R.id.email);
        password= findViewById(R.id.password);
        phone= findViewById(R.id.phone);
        address= findViewById(R.id.address);
        alterlog=findViewById(R.id.alterLogIn);
        signup=findViewById(R.id.signup);

        address.setAdapter(new PostedVoteAdapter(getApplicationContext(), R.layout.tag_places_list));

        address.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String locationtitle = (String) parent.getItemAtPosition(position);

                try{
                    getAddLatLon ss = new getAddLatLon();
                    myPosition= ss.execute(predsJsonArray.getJSONObject(position).getString("place_id")).get();
                    latitude=myPosition.latitude;
                    longitude=myPosition.longitude;

                }catch (Exception e){
                    e.printStackTrace();
                }
                address.setText(locationtitle);
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }

        });
        apiInterface = ApiClient.getApiClient().create(ApiInterface.class);

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String names,phones,emails,addresss,passwords;
                names=name.getText().toString();
                phones=phone.getText().toString();
                emails=email.getText().toString();
                passwords=password.getText().toString();
                addresss=address.getText().toString();
                if (names.equals("")||phones.equals("")||emails.equals("")||addresss.equals("")||passwords.equals(""))
                {
                    Toast.makeText(getApplicationContext(),"All field should be filled",Toast.LENGTH_LONG).show();
                }else {

                    Call<Status> call=apiInterface.userSignUp(emails,passwords,names,phones,addresss);
                    call.enqueue(new Callback<Status>() {
                        @Override
                        public void onResponse(Call<Status> call, Response<Status> response) {
                            Status res=response.body();
                            if (!res.getStatus().equals("0"))
                            {
                                Toast.makeText(getApplicationContext(),"sign up sucessfully ",Toast.LENGTH_LONG).show();
                                Call<Status> statusCall=apiInterface.setLocation(res.getStatus(),String.valueOf(latitude),String.valueOf(longitude),"0",addresss);
                                statusCall.enqueue(new Callback<Status>() {
                                    @Override
                                    public void onResponse(Call<Status> call, Response<Status> response) {
                                        Status loc=response.body();

                                    }

                                    @Override
                                    public void onFailure(Call<Status> call, Throwable t) {

                                    }
                                });
                                startActivity(new Intent(SignUp.this,LogIn.class));



                            }else {
                                name.setText("");
                                email.setText("");
                                phone.setText("");
                                password.setText("");
                                address.setText("");
                            }
                        }

                        @Override
                        public void onFailure(Call<Status> call, Throwable t) {

                        }
                    });
                }

            }
        });


    }



    class PostedVoteAdapter extends ArrayAdapter<String> implements Filterable {
        private ArrayList<String> resultList;

        public PostedVoteAdapter(Context context, int textViewResourceId) {
            super(context, textViewResourceId);
        }

        @Override
        public int getCount() {
            return resultList.size();
        }

        @Override
        public String getItem(int index) {
            return resultList.get(index);
        }

        @Override
        public Filter getFilter() {
            Filter filter = new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults filterResults = new FilterResults();
                    if (constraint != null) {
                        // Retrieve the autocomplete results.
                        resultList = autocomplete(constraint.toString());

                        // Assign the data to the FilterResults
                        filterResults.values = resultList;
                        filterResults.count = resultList.size();
                    }
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    if (results != null && results.count > 0) {
                        Log.e("results", String.valueOf(results));
                        notifyDataSetChanged();
                    } else {
                        notifyDataSetInvalidated();
                    }
                }
            };
            return filter;
        }

        public ArrayList<String> autocomplete(String input) {

            ArrayList<String> resultList = null;

            HttpURLConnection conn = null;
            StringBuilder jsonResults = new StringBuilder();
            try {
                StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
                sb.append("?key=" + API_KEY);
                //sb.append("&components=country:QA");
                sb.append("&input=" + URLEncoder.encode(input, "utf8"));

                URL url = new URL(sb.toString());

                System.out.println("URL: "+url);
                conn = (HttpURLConnection) url.openConnection();
                InputStreamReader in = new InputStreamReader(conn.getInputStream());

                // Load the results into a StringBuilder
                int read;
                char[] buff = new char[1024];
                while ((read = in.read(buff)) != -1) {
                    jsonResults.append(buff, 0, read);
                }
            } catch (MalformedURLException e) {
                return resultList;
            } catch (IOException e) {
                return resultList;
            } finally {
                if (conn != null) {
                    conn.disconnect();
                }
            }

            try {

                // Create a JSON object hierarchy from the results
                JSONObject jsonObj = new JSONObject(jsonResults.toString());
                predsJsonArray = jsonObj.getJSONArray("predictions");
                Log.e("jsonObj", String.valueOf(jsonObj));

                // Extract the Place descriptions from the results
                resultList = new ArrayList<String>(predsJsonArray.length());
                placeids = new JSONArray();
                for (int i = 0; i < predsJsonArray.length(); i++) {

                    System.out.println(predsJsonArray.getJSONObject(i).getString("place_id"));
                    System.out.println(predsJsonArray.getJSONObject(i).getString("description"));
                    System.out.println(predsJsonArray.getJSONObject(i).getString("terms"));
                    System.out.println("============================================================");
                    //resultList.add(predsJsonArray.getJSONObject(i).getString("description")+ "              #" +predsJsonArray.getJSONObject(i).getString("place_id"));
                    resultList.add(predsJsonArray.getJSONObject(i).getString("description"));

                }
            } catch (JSONException e) {
            }

            return resultList;
        }


    }





    class getAddLatLon extends AsyncTask<String, String, LatLng> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected LatLng doInBackground(String... para) {

            JSONObject json=jparser.makeHttpRequest("https://maps.googleapis.com/maps/api/place/details/json?placeid="+para[0]+"&key="+API_KEY, "GET",null);
            LatLng latlng = null;
            try {
                JSONObject jso = new JSONObject(json.toString());
                latlng= new LatLng(jso.getJSONObject("result").getJSONObject("geometry").getJSONObject("location").getDouble("lat"),
                        jso.getJSONObject("result").getJSONObject("geometry").getJSONObject("location").getDouble("lng"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return latlng;
        }

        @Override
        protected void onPostExecute(LatLng result) {
            super.onPostExecute(result);

        }
    }






}
