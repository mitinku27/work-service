package com.developer.InstantAssistanceSeeking;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.developer.InstantAssistanceSeeking.Model.Data;
import com.developer.InstantAssistanceSeeking.User.UserHome;
import com.developer.InstantAssistanceSeeking.Worker.WorkerHome;
import com.skyfishjy.library.RippleBackground;

public class MainActivity extends AppCompatActivity {
     RippleBackground rippleBackground;
     SharedPref sharedPref;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

         rippleBackground=(RippleBackground)findViewById(R.id.content);
        rippleBackground.startRippleAnimation();
        sharedPref=new SharedPref(this);
       /* TextView imageView=findViewById(R.id.text);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

*/
        final String data=sharedPref.getProfile();
        final Intent myintent= new Intent(this,UserChooser.class);

        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                if (data.equals("0"))
                {
                    startActivity(myintent);
                }else if(data.equals("1")){
                    startActivity(new Intent(MainActivity.this, WorkerHome.class));
                }else {
                    startActivity(new Intent(MainActivity.this, UserHome.class));
                }

                finish();
            }
        }, 2600);

    }

    @Override
    protected void onPause() {
        super.onPause();
        rippleBackground.stopRippleAnimation();
    }
}
