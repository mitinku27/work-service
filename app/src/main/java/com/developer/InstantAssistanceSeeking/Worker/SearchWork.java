package com.developer.InstantAssistanceSeeking.Worker;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.developer.InstantAssistanceSeeking.Model.AllWorkModel;
import com.developer.InstantAssistanceSeeking.Model.Data;
import com.developer.InstantAssistanceSeeking.Model.NewWorkDetail;
import com.developer.InstantAssistanceSeeking.Model.Status;
import com.developer.InstantAssistanceSeeking.R;
import com.developer.InstantAssistanceSeeking.Retrofit.ApiClient;
import com.developer.InstantAssistanceSeeking.Retrofit.ApiInterface;
import com.developer.InstantAssistanceSeeking.SharedPref;
import com.developer.InstantAssistanceSeeking.User.NearestWorkerInmap;
import com.developer.InstantAssistanceSeeking.User.UserHome;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchWork extends FragmentActivity implements OnMapReadyCallback , GoogleApiClient.ConnectionCallbacks
        , GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private GoogleMap mMap;
    GoogleApiClient client;
    LocationRequest request;
    LatLng latLng;
    int counter = -1;
    List<AllWorkModel> allWorkModelList;
    ApiInterface apiInterface;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_work);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        apiInterface= ApiClient.getApiClient().create(ApiInterface.class);
        allWorkModelList=new ArrayList<>();
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap = googleMap;

        client = new GoogleApiClient.Builder(getApplicationContext()).addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        client.connect();
        // Add a marker in Sydney and move the camera

        Call<List<AllWorkModel>> call=apiInterface.getAllWork();
        call.enqueue(new Callback<List<AllWorkModel>>() {
            @Override
            public void onResponse(Call<List<AllWorkModel>> call, Response<List<AllWorkModel>> response) {
                allWorkModelList=response.body();
                for (int i=0;i<allWorkModelList.size();i++)
                {
                    AllWorkModel model=allWorkModelList.get(i);
                    String location=model.getLocation();
                    String lat=location.substring(0,location.indexOf("@"));
                    String lng=location.substring(location.indexOf("@")+1);
                    String head=model.getName()+"#"+model.getMoney()+"@"+model.getPhone();
                    LatLng sydney = new LatLng(Double.valueOf(lat),Double.valueOf(lng));
                    SharedPref sharedPref=new SharedPref(SearchWork.this);

                    Data data=sharedPref.getMember();
                    if (data.getType_id().equals(model.getTypeId()))
                    {
                        mMap.addMarker(new MarkerOptions().position(sydney)
                                .title(head)
                                .snippet(model.getId()+"#"+model.getUserId())
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.engineer)));

                    }

                }

            }

            @Override
            public void onFailure(Call<List<AllWorkModel>> call, Throwable t) {

            }
        });


        mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

            // Use default InfoWindow frame
            @Override
            public View getInfoWindow(Marker args) {
                return null;
            }


            // Defines the contents of the InfoWindow
            @Override
            public View getInfoContents(final Marker args) {

                // Getting view from the layout file info_window_layout
                View v = getLayoutInflater().inflate(R.layout.custom_window, null);
                TextView title = (TextView) v.findViewById(R.id.tvTitle);
                TextView ph = (TextView) v.findViewById(R.id.phone);
                TextView ad = (TextView) v.findViewById(R.id.address);
                String take=args.getTitle();
                String name=take.substring(0,take.indexOf("#"));
                String mon=take.substring(take.indexOf("#")+1,take.indexOf("@"));
                String pho=take.substring(take.indexOf("@")+1);
                title.setText("name: "+name);
                ph.setText("phone: "+pho);
                ad.setText("amount: "+mon);
                // Returning the view containing InfoWindow contents
                return v;

            }
        });

        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener()
        {

            @Override
            public void onInfoWindowClick(Marker thisMarker)
            {
                final Dialog dialog = new Dialog(SearchWork.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setCancelable(true);
                dialog.setContentView(R.layout.dialog);
                Button dialogButton = (Button) dialog.findViewById(R.id.btn_dialog);
                final SharedPref sharedPref=new SharedPref(SearchWork.this);
                final Data data=sharedPref.getMember();
                TextView text = (TextView) dialog.findViewById(R.id.text_dialog);
                text.setText("Are You Sure to Send a Request?");
                String hh=thisMarker.getSnippet();
                final String work_id=hh.substring(0,hh.indexOf("#"));
                final String user_id=hh.substring(hh.indexOf("#")+1);

                NewWorkDetail newWorkDetail=sharedPref.getWorkDetail();
                if (newWorkDetail!=null)
                {
                    dialogButton.setVisibility(View.GONE);
                    dialogButton.setClickable(false);
                    text.setText("you already have a request");
                }
                else {
                    dialogButton.setVisibility(View.VISIBLE);
                    dialogButton.setClickable(true);
                }
                String in= sharedPref.getInterest();
                if (in.equals(work_id)){
                    dialogButton.setVisibility(View.GONE);
                    dialogButton.setClickable(false);
                    text.setText("you already sent a request");
                }else {
                    dialogButton.setVisibility(View.VISIBLE);
                    dialogButton.setClickable(true);
                }

                dialogButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                       Call<Status> statusCall=apiInterface.setInterest(data.getId(),work_id,user_id,"1");
                       statusCall.enqueue(new Callback<Status>() {
                           @Override
                           public void onResponse(Call<Status> call, Response<Status> response) {
                               Status status=response.body();
                               if (status.getStatus().equals("1"))
                               {
                                   sharedPref.setInterest(work_id);
                                   startActivity(new Intent(SearchWork.this, WorkerHome.class));

                               }
                           }

                           @Override
                           public void onFailure(Call<Status> call, Throwable t) {

                           }
                       });
                    }
                });

                dialog.show();
            }
        });

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        request = new LocationRequest().create();

        request.setFastestInterval(5000);
        request.setInterval(5000);
        request.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(SearchWork.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);

        }
        LocationServices.FusedLocationApi.requestLocationUpdates(client, request, SearchWork.this);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        if (location == null) {
            Toast.makeText(getApplicationContext(), "location not found", Toast.LENGTH_LONG).show();
        } else {

            latLng = new LatLng(location.getLatitude(), location.getLongitude());
            counter++;
            if (counter == 0) {
                CameraUpdate center =
                        CameraUpdateFactory.newLatLng(new LatLng(latLng.latitude,
                                latLng.longitude));
                CameraUpdate zoom = CameraUpdateFactory.zoomTo(12);
                mMap.moveCamera(center);
                mMap.animateCamera(zoom);
               /* MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.flag));
                markerOptions.position(latLng);
                markerOptions.title("market at home");
                mMap.addMarker(markerOptions);*/
            }


        }
    }
}
