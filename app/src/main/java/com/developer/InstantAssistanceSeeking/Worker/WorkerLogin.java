package com.developer.InstantAssistanceSeeking.Worker;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.developer.InstantAssistanceSeeking.Model.UserLogInInfo;
import com.developer.InstantAssistanceSeeking.R;
import com.developer.InstantAssistanceSeeking.Retrofit.ApiClient;
import com.developer.InstantAssistanceSeeking.Retrofit.ApiInterface;
import com.developer.InstantAssistanceSeeking.SharedPref;
import com.developer.InstantAssistanceSeeking.User.LogIn;
import com.developer.InstantAssistanceSeeking.User.SignUp;
import com.developer.InstantAssistanceSeeking.User.UserHome;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WorkerLogin extends AppCompatActivity {
    TextView alterSignUp;
    Button signin;
    EditText email,password;
    ApiInterface apiInterface;
    SharedPref sharedPref;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_worker_login);
        alterSignUp=findViewById(R.id.alterSignUp);
        signin=findViewById(R.id.signin);



        email=findViewById(R.id.email);
        password=findViewById(R.id.password);
        apiInterface= ApiClient.getApiClient().create(ApiInterface.class);
        alterSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(WorkerLogin.this,WorkerSignUp.class));
            }
        });

        sharedPref=new SharedPref(getApplicationContext());
        signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String e=email.getText().toString();
                String pass=password.getText().toString();
                Call<UserLogInInfo> call=apiInterface.workerLogin(e,pass);
                call.enqueue(new Callback<UserLogInInfo>() {
                    @Override
                    public void onResponse(Call<UserLogInInfo> call, Response<UserLogInInfo> response) {
                        UserLogInInfo inInfo=response.body();
                        if (inInfo.getData()!=null)
                        {
                            sharedPref.setMember(inInfo.getData());
                            sharedPref.setProfile("1");
                            startActivity(new Intent(WorkerLogin.this, WorkerHome.class));
                            finish();
                        }

                    }

                    @Override
                    public void onFailure(Call<UserLogInInfo> call, Throwable t) {

                    }
                });


            }
        });
    }
}
