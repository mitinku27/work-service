package com.developer.InstantAssistanceSeeking.Worker;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.developer.InstantAssistanceSeeking.Model.Data;
import com.developer.InstantAssistanceSeeking.Model.Status;
import com.developer.InstantAssistanceSeeking.Model.Type;
import com.developer.InstantAssistanceSeeking.Model.WorkerDetailsModel;
import com.developer.InstantAssistanceSeeking.R;
import com.developer.InstantAssistanceSeeking.Retrofit.ApiClient;
import com.developer.InstantAssistanceSeeking.Retrofit.ApiInterface;
import com.developer.InstantAssistanceSeeking.SharedPref;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WorkerProfile extends AppCompatActivity {
    TextView name,email,phone,type;
    EditText salary,worktime,experience;
    ApiInterface apiInterface;

    List<Type> typeList=new ArrayList<>();
    Button submit;

    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_worker_profile);
        Toolbar userHomeToolbar;
        userHomeToolbar=findViewById(R.id.toolbar);
        userHomeToolbar.setTitle("Profile");
        userHomeToolbar.setTitleTextColor(getResources().getColor(R.color.cardview_light_background));
        userHomeToolbar.setNavigationIcon(R.drawable.ic_keyboard_backspace_black_24dp);
        setSupportActionBar(userHomeToolbar);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Spinner dropdown = findViewById(R.id.spinner1);
        apiInterface= ApiClient.getApiClient().create(ApiInterface.class);
        name=findViewById(R.id.name);
        email=findViewById(R.id.email);
        phone=findViewById(R.id.phone);
        type=findViewById(R.id.type);
        salary=findViewById(R.id.salary);
        worktime=findViewById(R.id.worktime);
        experience=findViewById(R.id.experience);
        submit=findViewById(R.id.submit);

        final SharedPref sharedPref= new SharedPref(this);
        final Data data=sharedPref.getMember();
        name.setText(data.getName());
        email.setText(data.getEmail());
        phone.setText(data.getPhone());

        Call<List<Type>> call=apiInterface.getTypeName(data.getType_id());

        call.enqueue(new Callback<List<Type>>() {
            @Override
            public void onResponse(Call<List<Type>> call, Response<List<Type>> response) {
                typeList=response.body();
                for (int i=0;i<typeList.size();i++)
                {
                    Type t= typeList.get(i);
                    type.setText(t.getName());
                }
            }

            @Override
            public void onFailure(Call<List<Type>> call, Throwable t) {

            }
        });
        String pro=sharedPref.getPro();
        if (!pro.equals("0"))
        {
            submit.setVisibility(View.GONE);
            submit.setClickable(false);
            Call<WorkerDetailsModel> statusCall= apiInterface.getWorkerDetails(data.getId());
            statusCall.enqueue(new Callback<WorkerDetailsModel>() {
                @Override
                public void onResponse(Call<WorkerDetailsModel> call, Response<WorkerDetailsModel> response) {
                    WorkerDetailsModel detailsModel=response.body();
                    experience.setText(detailsModel.getExperience());
                    salary.setText(detailsModel.getExpectedSalary());
                    worktime.setText(detailsModel.getWorkingHours());
                }

                @Override
                public void onFailure(Call<WorkerDetailsModel> call, Throwable t) {

                }
            });
        }


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!experience.getText().toString().equals("")|| !salary.getText().toString().equals("")
                || !worktime.getText().toString().equals("")){
                    Call<Status> statusCall= apiInterface.setWorkerDetails(data.getId(),experience.getText().toString(),salary.getText().toString(),worktime.getText().toString());
                    statusCall.enqueue(new Callback<Status>() {
                        @Override
                        public void onResponse(Call<Status> call, Response<Status> response) {
                            Status status=response.body();
                            if (status.getStatus().equals("1"))
                            {
                                sharedPref.setPro("1");
                                startActivity(new Intent(WorkerProfile.this,WorkerHome.class));
                            }

                        }

                        @Override
                        public void onFailure(Call<Status> call, Throwable t) {

                        }
                    });
                }
        }


    });
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

}
