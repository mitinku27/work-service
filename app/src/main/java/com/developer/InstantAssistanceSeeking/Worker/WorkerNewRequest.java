package com.developer.InstantAssistanceSeeking.Worker;

import android.annotation.SuppressLint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.developer.InstantAssistanceSeeking.Model.Data;
import com.developer.InstantAssistanceSeeking.Model.Interest;
import com.developer.InstantAssistanceSeeking.Model.NewWork;
import com.developer.InstantAssistanceSeeking.Model.NewWorkDetail;
import com.developer.InstantAssistanceSeeking.Model.UserLogInInfo;
import com.developer.InstantAssistanceSeeking.Model.workDetailModel;
import com.developer.InstantAssistanceSeeking.R;
import com.developer.InstantAssistanceSeeking.Retrofit.ApiClient;
import com.developer.InstantAssistanceSeeking.Retrofit.ApiInterface;
import com.developer.InstantAssistanceSeeking.SharedPref;
import com.developer.InstantAssistanceSeeking.User.CateGoryDetails;
import com.developer.InstantAssistanceSeeking.User.FoldingCellListAdapter;
import com.developer.InstantAssistanceSeeking.User.WorkerDetailsModel;
import com.ramotion.foldingcell.FoldingCell;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WorkerNewRequest extends AppCompatActivity {
    ApiInterface apiInterface;
    RecyclerView recyclerView;
    List<NewWorkDetail> newWorkDetailList=new ArrayList<>();

    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_worker_new_request);

        Toolbar userHomeToolbar;
        userHomeToolbar=findViewById(R.id.toolbar);
        recyclerView=findViewById(R.id.rev);
        userHomeToolbar.setTitle("New Request");
        userHomeToolbar.setTitleTextColor(getResources().getColor(R.color.cardview_light_background));
        userHomeToolbar.setNavigationIcon(R.drawable.ic_keyboard_backspace_black_24dp);
        setSupportActionBar(userHomeToolbar);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        apiInterface= ApiClient.getApiClient().create(ApiInterface.class);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        SharedPref sharedPref=new SharedPref(this);
        final Data data=sharedPref.getMember();

        Call<List<NewWorkDetail>> call=apiInterface.workerRequst(data.getId());

        call.enqueue(new Callback<List<NewWorkDetail>>() {
            @Override
            public void onResponse(Call<List<NewWorkDetail>> call, Response<List<NewWorkDetail>> response) {
                newWorkDetailList=response.body();
                NewRequestRecycler requestRecycler=new NewRequestRecycler(WorkerNewRequest.this,newWorkDetailList);
                recyclerView.setAdapter(requestRecycler);
            }

            @Override
            public void onFailure(Call<List<NewWorkDetail>> call, Throwable t) {

            }
        });



    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

}
