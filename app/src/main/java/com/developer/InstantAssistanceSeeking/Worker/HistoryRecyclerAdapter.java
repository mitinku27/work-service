package com.developer.InstantAssistanceSeeking.Worker;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.developer.InstantAssistanceSeeking.Model.Data;
import com.developer.InstantAssistanceSeeking.Model.NewWorkDetail;
import com.developer.InstantAssistanceSeeking.Model.User;
import com.developer.InstantAssistanceSeeking.Model.Work;
import com.developer.InstantAssistanceSeeking.R;
import com.developer.InstantAssistanceSeeking.SharedPref;

import java.util.List;

public class HistoryRecyclerAdapter extends RecyclerView.Adapter<HistoryRecyclerAdapter.ViewHolder> {
    Context context;
    List<NewWorkDetail> newWorkDetailList;
    public HistoryRecyclerAdapter(Context context, List<NewWorkDetail> newWorkDetailList) {
        this.context=context;
        this.newWorkDetailList=newWorkDetailList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_worker_history,viewGroup,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        NewWorkDetail newWorkDetail=newWorkDetailList.get(i);
        SharedPref sharedPref=new SharedPref(context);
        Data data= sharedPref.getMember();
        User user=newWorkDetail.getUser();
        Work work=newWorkDetail.getWork();
        viewHolder.username.setText(user.getName());
        viewHolder.workername.setText(data.getName());
        viewHolder.description.setText(work.getWorkDescription());
        viewHolder.payment.setText(work.getMoney()+" taka");
        viewHolder.days.setText(work.getTime()+" day");
    }

    @Override
    public int getItemCount() {
        return newWorkDetailList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView username,workername,description,payment,days;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            username=itemView.findViewById(R.id.username);
            workername=itemView.findViewById(R.id.workername);
            description=itemView.findViewById(R.id.description);
            payment=itemView.findViewById(R.id.payment);
            days=itemView.findViewById(R.id.days);
        }
    }
}
