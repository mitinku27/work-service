package com.developer.InstantAssistanceSeeking.Worker;

import android.annotation.SuppressLint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.developer.InstantAssistanceSeeking.Model.Data;
import com.developer.InstantAssistanceSeeking.Model.NewWorkDetail;
import com.developer.InstantAssistanceSeeking.R;
import com.developer.InstantAssistanceSeeking.Retrofit.ApiClient;
import com.developer.InstantAssistanceSeeking.Retrofit.ApiInterface;
import com.developer.InstantAssistanceSeeking.SharedPref;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WorkerHistory extends AppCompatActivity {
    ApiInterface apiInterface;
    List<NewWorkDetail> newWorkDetailList=new ArrayList<>();
    SharedPref sharedPref;
    RecyclerView rev;
    @SuppressLint("RestrictedApi")

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_worker_history);

       rev=findViewById(R.id.rev);
        sharedPref=new SharedPref(this);
        Toolbar userHomeToolbar;
        userHomeToolbar=findViewById(R.id.toolbar);
        userHomeToolbar.setTitle("History");
        userHomeToolbar.setTitleTextColor(getResources().getColor(R.color.cardview_light_background));
        userHomeToolbar.setNavigationIcon(R.drawable.ic_keyboard_backspace_black_24dp);
        setSupportActionBar(userHomeToolbar);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        rev.setHasFixedSize(true);
        rev.setLayoutManager(new LinearLayoutManager(this));
        apiInterface= ApiClient.getApiClient().create(ApiInterface.class);
        Data data=sharedPref.getMember();
        Call<List<NewWorkDetail>> call=apiInterface.workerReport(data.getId());
        call.enqueue(new Callback<List<NewWorkDetail>>() {
        @Override
        public void onResponse(Call<List< NewWorkDetail >> call, Response<List<NewWorkDetail>> response) {
                newWorkDetailList=response.body();
                HistoryRecyclerAdapter adapter=new HistoryRecyclerAdapter(WorkerHistory.this,newWorkDetailList);
                rev.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<List<NewWorkDetail>> call, Throwable t) {

            }

        });



    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }
}
