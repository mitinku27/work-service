package com.developer.InstantAssistanceSeeking;

import android.content.Context;
import android.content.SharedPreferences;

import com.developer.InstantAssistanceSeeking.Model.Data;
import com.developer.InstantAssistanceSeeking.Model.NewWorkDetail;
import com.google.gson.Gson;

public class SharedPref {
    Context context;
    public SharedPref(Context context) {
        this.context = context;
    }

    public void setMember(Data member)
    {
        SharedPreferences sharedPref = context.getSharedPreferences("test",0);
        SharedPreferences.Editor prefsEditor = sharedPref.edit();
        Gson gson = new Gson();
        String json = gson.toJson(member);
        prefsEditor.putString("MyObject", json);
        prefsEditor.commit();
    }
    public Data getMember()
    {
        SharedPreferences sharedPref = context.getSharedPreferences("test",0);
        Gson gson = new Gson();
        String json = sharedPref.getString("MyObject", "");
        Data obj = gson.fromJson(json, Data.class);
        return obj;
    }
    public void setWork(String work)
    {
        SharedPreferences sharedPref = context.getSharedPreferences("test",0);
        SharedPreferences.Editor prefsEditor = sharedPref.edit();
        prefsEditor.putString("work",work);
        prefsEditor.commit();
    }
    public String getWork()
    {
        SharedPreferences sharedPref = context.getSharedPreferences("test",0);
        String work=sharedPref.getString("work","0");
        return work;
    }

    public void setProfile(String work)
    {
        SharedPreferences sharedPref = context.getSharedPreferences("test",0);
        SharedPreferences.Editor prefsEditor = sharedPref.edit();
        prefsEditor.putString("pro",work);
        prefsEditor.commit();
    }
    public String getProfile()
    {
        SharedPreferences sharedPref = context.getSharedPreferences("test",0);
        String work=sharedPref.getString("pro","0");
        return work;
    }
    public void setInterest(String work)
    {
        SharedPreferences sharedPref = context.getSharedPreferences("test",0);
        SharedPreferences.Editor prefsEditor = sharedPref.edit();
        prefsEditor.putString("interest",work);
        prefsEditor.commit();
    }
    public String getInterest()
    {
        SharedPreferences sharedPref = context.getSharedPreferences("test",0);
        String work=sharedPref.getString("interest","0");
        return work;
    }

    public void setPro(String work)
    {
        SharedPreferences sharedPref = context.getSharedPreferences("test",0);
        SharedPreferences.Editor prefsEditor = sharedPref.edit();
        prefsEditor.putString("setpro",work);
        prefsEditor.commit();
    }
    public String getPro()
    {
        SharedPreferences sharedPref = context.getSharedPreferences("test",0);
        String work=sharedPref.getString("setpro","0");
        return work;
    }

    public void setWorkDetail(NewWorkDetail member)
    {
        SharedPreferences sharedPref = context.getSharedPreferences("test",0);
        SharedPreferences.Editor prefsEditor = sharedPref.edit();
        Gson gson = new Gson();
        String json = gson.toJson(member);
        prefsEditor.putString("obwork", json);
        prefsEditor.commit();
    }
    public NewWorkDetail getWorkDetail()
    {
        SharedPreferences sharedPref = context.getSharedPreferences("test",0);
        Gson gson = new Gson();
        String json = sharedPref.getString("obwork", "");
        NewWorkDetail obj = gson.fromJson(json, NewWorkDetail.class);
        return obj;
    }


}
